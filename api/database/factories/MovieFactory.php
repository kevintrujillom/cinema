<?php

namespace Database\Factories;

use App\Models\Movie;
use Illuminate\Database\Eloquent\Factories\Factory;

class MovieFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Movie::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $images = [
            'images/movie-1.jpg',
            'images/movie-2.jpg',
            'images/movie-3.jpg',
            'images/movie-4.jpg'
        ];

        $image_number = rand(0, 3);
        $brand_number = rand(1, 5);

        return [
            'code'        => $this->faker->unique()->text(10),
            'name'        => $this->faker->name,
            'description' => $this->faker->text(150),
            'image'       => $images[$image_number],
            'cinema_id'    => $brand_number,
        ];
    }
}
