export class Cinema {
    id: number;
    name: string;
    capacity: number;
}
