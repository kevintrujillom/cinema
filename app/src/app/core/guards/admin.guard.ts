import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {

  user: any = [];

  constructor(  private router: Router ) { }

  canActivate(): boolean {

    this.user = JSON.parse(localStorage.getItem('user_carvajal'));

    if (this.user) {

      if (this.user.name === 'Administrador') {
  
        return true;
  
      } if (this.user.name === 'Usuario') {
  
        this.router.navigate(['/']);
        return false;
  
      } else {
  
        this.router.navigate(['login']);
        return false;
      }

    } else {
      
      this.router.navigate(['/']);
      return false;      
    }


  }  
  
}
