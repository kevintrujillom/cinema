import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';

import { Cinema } from './../../../../core/models/cinema.model';
import { CinemaService } from './../../../../core/services/cinema.service';
import { Movie } from './../../../../core/models/movie.model';
import { MovieService } from './../../../../core/services/movie.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from './../../../../../environments/environment';

@Component({
  selector: 'app-movie-edit',
  templateUrl: './movie-edit.component.html',
  styleUrls: ['./movie-edit.component.scss']
})
export class MovieEditComponent implements OnInit {

  movie: Movie = {
    id: 0,
    code: '',
    name: '',
    description: '',
    image: '',
    cinema_id: 0
  };

  cinemas: Cinema[] = [];
  formEdit: FormGroup;
  selectedValue = 0;
  selectedFile: File = null;
  imageMin = null;
  urlImages = environment.path_images;

  constructor( 
    private route: ActivatedRoute,
    private cinemaService: CinemaService, 
    private movieService: MovieService, 
    private toastr: ToastrService) 
  {
    this.formEdit = this.createFormGroup();
  }

  ngOnInit(): void {

    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      this.getMovie(id);
    });
    
    this.getCinemas();

  }

  createFormGroup(){
    return new FormGroup({
      code: new FormControl('', [ Validators.required ]),
      name: new FormControl('', [ Validators.required ]),
      description: new FormControl('', [ Validators.required ]),
      cinema_id: new FormControl('', [ Validators.required ])
    });
  }  

  getMovie(id: number){

    this.movieService.getByid(id).subscribe(movie => {

      this.movie = movie.response;
      this.selectedValue = this.movie.cinema_id;

      this.formEdit.patchValue({
        code: this.movie.code,
        name: this.movie.name,
        description: this.movie.description,
        cinema_id: this.movie.cinema_id
      });

    });

  }  

  getCinemas(){
    this.cinemaService.getAll().subscribe(cinemas => {
      this.cinemas = cinemas.response
    });
  }  

  save(id: number){

    let data = null;
    let hasFile = false;

    if (this.formEdit.valid) {

      if (this.selectedFile) {

        data = this.formData();
        hasFile = true;
      
      } else {

        data = this.formEdit.value;
      }

      this.movieService.update(id, data, hasFile).subscribe(
        data => {
          this.showMessage(true, data.response);
        },
        error => {
          this.showMessage(false, 'Error interno al actualizarla.');
        }
      );

    } else {

      this.showMessage(false, 'Debes llenar todos los campos');
    }

  }  

  formData() {
    const formData = new FormData();
    formData.append('_method', 'PUT');
    formData.append('code', this.formEdit.controls.code.value);
    formData.append('name', this.formEdit.controls.name.value);
    formData.append('description', this.formEdit.controls.description.value);
    formData.append("image", this.selectedFile);
    formData.append('cinema_id', this.formEdit.controls.cinema_id.value);
    return formData;
  }

  getImage(event){
    this.selectedFile = <File>event.target.files[0];
    this.loadImage(this.selectedFile);
  }

  loadImage (file) {
    const reader = new FileReader()
    reader.onload = (e: any) => {
      this.imageMin = e.target.result
    }
    reader.readAsDataURL(file)
  }   

  showMessage(success: boolean, message: string = '') {

    if (success) {

      this.toastr.success(message, 'Película');

    } else {

      this.toastr.error(message, 'Película');
    }

  }  

  get name(): any { return this.formEdit.get('name'); }
  get code(): any { return this.formEdit.get('code'); }
  get description(): any { return this.formEdit.get('description'); }
  get cinema_id(): any { return this.formEdit.get('cinema_id'); }    

}
