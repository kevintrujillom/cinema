import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  
  user: any = [];

  constructor( private router: Router ) { }

  canActivate(): boolean {

    this.user = localStorage.getItem('user_carvajal');

    if (this.user) {

      return true;

    } else {

      this.router.navigate(['/']);
      return false;
    }
  }
}
