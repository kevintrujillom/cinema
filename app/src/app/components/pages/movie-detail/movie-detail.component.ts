import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';

import { Cinema } from './../../../core/models/cinema.model';
import { Movie } from './../../../core/models/movie.model';
import { MovieService } from './../../../core/services/movie.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from './../../../../environments/environment';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit {

  movie: Movie = {
    id: 0,
    code: '',
    name: '',
    description: '',
    image: '',
    cinema_id: 0
  };

  cinema: Cinema = {
      id: 0,
      name: '',
      capacity: 0
  };

  idMovie = 0;
  urlImages = environment.path_images;

  constructor( 
    private movieService: MovieService, 
    private route: ActivatedRoute,
    private toastr: ToastrService ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const id = params.id;
      this.idMovie = id;
      this.getMovie(id);
    });
  }

  getMovie(id: number){
    this.movieService.getByid(id).subscribe(movie => {
      this.movie = movie.response;
      this.cinema = movie.response.cinema;
    });
  }

  addList(){
    
    const userLogin = JSON.parse(localStorage.getItem('user_carvajal'));
    let list = JSON.parse(localStorage.getItem('movies_carvajal')) || false;
    
    if (userLogin) {

      if (!list) {

        list = this.idMovie;
      
      } else {

        list = list + '-' + this.idMovie;
      }
      
      localStorage.setItem('movies_carvajal', JSON.stringify(list));

      this.showMessage(true, 'Se agregó a tu lista con éxito .');

    } else {

      this.showMessage(false, 'Debes iniciar sesión para llevar a cabo esta acción.');
    }

  }

  showMessage(success: boolean, message: string = '') {

    if (success) {

      this.toastr.success(message, 'Película ');

    } else {

      this.toastr.error(message, 'Película ');
    }

  }      

}
