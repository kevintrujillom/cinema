import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Movie } from './../../../../core/models/movie.model';
import { MovieService } from './../../../../core/services/movie.service';
import { ToastrService } from 'ngx-toastr';
import { environment } from './../../../../../environments/environment';

import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from './../../../shared/dialog/dialog.component';

@Component({
  selector: 'app-movie-index',
  templateUrl: './movie-index.component.html',
  styleUrls: ['./movie-index.component.scss']
})
export class MovieIndexComponent implements OnInit {

  movies: Movie[] = [];
  moviesOriginal = [];
  moviesCopy = [];  
  actualPage: number = 1;

  search = '';
  activeSearch = false;
  movieCreated: boolean = false;
  urlImages = environment.path_images;

  formSearch: FormGroup;

  constructor( 
    private movieService: MovieService, 
    private toastr: ToastrService,
    public dialog: MatDialog ) {
    this.formSearch = this.searchFormGroup();
  }

  ngOnInit(): void {
    this.getAll();
  }

  ngOnChanges(): void {
    if (this.movieCreated) this.getAll();
  }  

  searchFormGroup(){
    return new FormGroup({
      movie: new FormControl('', [ Validators.required ])
    });
  }

  getAll(){
    this.movieService.getAll().subscribe(movies => {
      this.movies = movies.response;
    });
  }

  searchMovies(){
    
    this.search = this.formSearch.value.movie;

    if (this.activeSearch) this.movies = this.moviesOriginal;

    if (this.search.length > 0) {

      this.createCopyMovies();
  
      const nMoviess = this.movies.filter(movies => {
        return movies.name.toLowerCase().includes(this.search);
      });
  
      this.movies = nMoviess;

    } else {

      this.showMessage(false, 'Debes ingresar un parametro de búsqueda');
    }
    
  }

  delete(movie: Movie){
    const dialogRef = this.dialog.open(DialogComponent);
    dialogRef.afterClosed().subscribe(response => {
      if (response) this.confirmDelete(movie.id);
    });
  }

  confirmDelete(id: number){

    this.movieService.delete(id).subscribe(
      data => {
        this.showMessage(true, data.response);
        this.getAll();
      },
      error => {
        this.showMessage(false, 'Error interno al eliminarla.');
      }
    );

  }

  createCopyMovies(){
    this.moviesCopy = this.movies;
    this.moviesOriginal = this.movies;
    this.activeSearch = true;
  }

  clearSearchMovies() {
    this.movies = this.moviesOriginal;
    this.activeSearch = false;
    this.search = '';
  }

  showMessage(success: boolean, message: string = '') {

    if (success) {

      this.toastr.success(message, 'Película');

    } else {

      this.toastr.error(message, 'Película');
    }

  }

  readMovieCreated(event: boolean){

    this.movieCreated = event;

    if (this.movieCreated) {
      this.getAll();
      this.emitChanges();
    }
  }

  emitChanges(){
    setTimeout(() => {
      this.movieCreated = false;
    }, 2000);
  }   

  get movie(): any { return this.formSearch.get('movie'); }  
}
