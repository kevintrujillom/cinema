<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;

class UserController extends Controller
{
    public function login(Request $request){

        if ($request->input('email') || $request->input('password')) {

            $user = User::where('email', $request->input('email'))->get();

            if ($user) {

                $pass = $user[0]['password'];

                if ( \Hash::check($request->get('password'), $pass) ) {

                    return response()->json([
                        'isLogin'  => true,
                        'response' => $user[0]
                    ], 200);                 

                } else {

                    return response()->json([
                        'isLogin'  => false,
                        'response' => "Datos de acceso incorrectos."
                    ], 400);                    
                }
            
            } else {

                return response()->json([
                    'isLogin'  => false,
                    'response' => "Datos de acceso incorrectos."
                ], 400);                 
            }

        } else {

            return response()->json([ 
                'response' => "Campos vacíos."
            ], 400); 
        }
    }

    public function logout(){

        return response()->json([
            'response' => true
        ], 200); 
  
    }
    
    public function register(Request $request) {

        if ($request->input('email') && $request->input('password')) {

            $existUser = User::where('email', $request->input('email'))->get();

            if (count($existUser) > 0) {

                return response()->json([
                    'existUser'  => true,
                    'cafs' => $existUser
                ], 200);

            } else {

                $password = \Hash::make($request->input('password'));

                $user = User::create([
                    'name' => 'Usuario',
                    'email' => $request->input('email'),
                    'password' => $password
                ]);
                
                if ($user) {
    
                    return response()->json([ 
                        'response' => $user,
                        '$password' => $password
                    ], 200);
                    
                } else {
        
                    return response()->json([ 
                        'response' => "{$user->name}, no registrado."
                    ], 400);              
                }                
             
            }

        } else {

            return response()->json([ 
                'response' => "Campos vacíos."
            ], 400); 
        }        
    }

    public function index()
    {
        $users = User::orderBy('id', 'DESC')->get();

        if ($users) {

            return response()->json([ 
                'response' => $users
            ], 200);

        }

        return response()->json([ 
            'response' => 'Sin resultados'
        ], 404);
    }    
}