import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from './../../../core/services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() isUserLogin: boolean;
  isLogin: boolean = false;
  isAdmin: boolean = false;

  constructor( 
    private authService: AuthService,
    private toastr: ToastrService,
    private router: Router ) { }

  ngOnInit(): void {
    this.validateLogin();
  }

  ngOnChanges(): void {
    if (this.isUserLogin) this.isLogin = true;
  }

  validateLogin(): void {

    setInterval(() => {

      const AuthUser = JSON.parse(localStorage.getItem('user_carvajal'));

      if (AuthUser) {
        this.isLogin = true;
        if (AuthUser.name == 'Administrador') this.isAdmin = true;
      }

    }, 2000);

  }

  logout(){

    this.authService.logout(true).subscribe(
      data => {
        if (data.response) {
          this.isLogin = false;
          this.isAdmin = false;
          localStorage.removeItem('user_carvajal');
          this.router.navigate(['login']);
        }
      },
      error => {
        this.showMessage(false, 'Erroe interno al cerrar sesión.');
      }
    );     
  }

  showMessage(success: boolean, message: string = '') {

    if (success) {

      this.toastr.success(message, 'Login');

    } else {

      this.toastr.error(message, 'Login');
    }

  }     
  
}
