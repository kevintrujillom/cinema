<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\ApiCinemasController;
use App\Http\Controllers\ApiMoviesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * API URL for auth.
 */
Route::post('/auth/login', [UserController::class, 'login']);
Route::post('/auth/logout', [UserController::class, 'logout']);
Route::post('/auth/register', [UserController::class, 'register']);

/**
 * API URL for cinemas table.
 */
Route::get('/cinemas', [ApiCinemasController::class, 'index']);
Route::get('/cinemas/{id}', [ApiCinemasController::class, 'show']);
Route::post('/cinemas', [ApiCinemasController::class, 'store']);
Route::delete('/cinemas/{id}', [ApiCinemasController::class, 'destroy']);
Route::put('/cinemas/{id}', [ApiCinemasController::class, 'update']);

/**
 * API URL for movies table.
 */
Route::get('/movies', [ApiMoviesController::class, 'index']);
Route::get('/movies-lista', [ApiMoviesController::class, 'list']);
Route::get('/movies/{id}', [ApiMoviesController::class, 'show']);
Route::post('/movies', [ApiMoviesController::class, 'store']);
Route::delete('/movies/{id}', [ApiMoviesController::class, 'destroy']);
Route::put('/movies/{id}', [ApiMoviesController::class, 'update']);

/**
 * API URL for users table.
 */
Route::get('/users', [UserController::class, 'index']);
