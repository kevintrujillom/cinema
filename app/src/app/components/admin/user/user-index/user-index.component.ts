import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { User } from './../../../../core/models/user.model';
import { UserService } from './../../../../core/services/user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-index',
  templateUrl: './user-index.component.html',
  styleUrls: ['./user-index.component.scss']
})
export class UserIndexComponent implements OnInit {

  users: User[] = [];
  usersOriginal = [];
  usersCopy = [];  
  actualPage: number = 1;

  search = '';
  activeSearch = false; 

  formSearch: FormGroup;

  constructor( private userService: UserService, private toastr: ToastrService ) { 
    this.formSearch = this.searchFormGroup();
  }

  ngOnInit(): void {
    this.getAll();    
  }

  searchFormGroup(){
    return new FormGroup({
      name: new FormControl('', [ Validators.required ])
    });
  }  

  getAll(){
    this.userService.getAll().subscribe(users => {
      this.users = users.response
    });
  }  

  searchUsers(){
    
    this.search = this.formSearch.value.name;

    if (this.activeSearch) this.users = this.usersOriginal;

    if (this.search.length > 0) {

      this.createCopyUsers();
  
      const nUsers = this.users.filter(user => {
        return user.email.toLowerCase().includes(this.search);
      });
  
      this.users = nUsers;

    } else {

      this.showMessage(false, 'Debes ingresar un parametro de búsqueda');
    }
    
  } 
   
  createCopyUsers(){
    this.usersCopy = this.users;
    this.usersOriginal = this.users;
    this.activeSearch = true;
  }

  clearSearchUsers() {
    this.users = this.usersOriginal;
    this.activeSearch = false;
    this.search = '';
  }

  showMessage(success: boolean, message: string = '') {

    if (success) {

      this.toastr.success(message, 'Usuarios');

    } else {

      this.toastr.error(message, 'Usuarios');
    }

  }    

}
