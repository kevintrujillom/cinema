import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cutString'
})
export class CutStringPipe implements PipeTransform {

  transform(value: string): any {

    if (value.length > 100) {

      return value.substr(0, 100) + '...';
    
    } else {

      return value;
    }

  }

}
