import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from './../../../core/services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({ 
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  formRegister: FormGroup;

  emailPattern: any = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

  constructor( 
    private authService: AuthService, 
    private toastr: ToastrService,
    private router: Router ) 
  { 
    this.formRegister = this.registerFormGroup();
  }

  ngOnInit(): void {
    this.isLogin();
  }

  registerFormGroup(){

    return new FormGroup({

      email: new FormControl('', [
        Validators.required,
        Validators.pattern(this.emailPattern)
      ]),

      password: new FormControl('', [
        Validators.required
      ])

    });

  } 
  
  isLogin(): void {

    const AuthUser = localStorage.getItem('user_carvajal');

    if (AuthUser) {
      this.router.navigate(['/']);
    }
  }

  register(): void {

    if (this.formRegister.valid){

      this.authService.register(this.formRegister.value).subscribe(
        data => {
          if (data.existUser) {

            this.showMessage(false, 'Este correo electrónico ya esta asociado a otra cuenta.');

          } else if (data.response) {

            localStorage.setItem('user_carvajal', JSON.stringify(data.response));
            this.showMessage(true, 'Bienvenido a Cinema!');         
            this.router.navigate(['/']);
          }
        },
        error => {
          localStorage.removeItem('user_carvajal');
          this.showMessage(false, 'Error interno, intentelo más tarde.');
        }
      );      

    } else {

      this.showMessage(false, 'Debes llenar todos los campos');
    }

  }

  showMessage(success: boolean, message: string = '') {

    if (success) {

      this.toastr.success(message, 'Registro');

    } else {

      this.toastr.error(message, 'Registro');
    }

  }    

  get email(): any { return this.formRegister.get('email'); }
  get password(): any { return this.formRegister.get('password'); }   

}
