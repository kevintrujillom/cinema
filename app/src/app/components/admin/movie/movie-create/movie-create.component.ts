import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Cinema } from './../../../../core/models/cinema.model';
import { CinemaService } from './../../../../core/services/cinema.service';
import { Movie } from './../../../../core/models/movie.model';
import { MovieService } from './../../../../core/services/movie.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-movie-create',
  templateUrl: './movie-create.component.html',
  styleUrls: ['./movie-create.component.scss']
})
export class MovieCreateComponent implements OnInit {

  @Output() movieCreated: EventEmitter<boolean> = new EventEmitter();

  cinemas: Cinema[] = [];
  formCreate: FormGroup;
  selectedFile: File = null;
  imageMin = null;

  constructor( 
    private cinemaService: CinemaService,
    private movieService: MovieService, 
    private toastr: ToastrService )
  {
    this.formCreate = this.createFormGroup();
  }

  ngOnInit(): void {
    this.getCinemas();
  }

  createFormGroup(){
    return new FormGroup({
      code: new FormControl('', [ Validators.required ]),
      name: new FormControl('', [ Validators.required ]),
      description: new FormControl('', [ Validators.required ]),
      image: new FormControl('', [ Validators.required ]),
      cinema_id: new FormControl('', [ Validators.required ])
    });
  }

  create(){

    if (this.formCreate.valid){

      const formData = this.formData();

      this.movieService.create(formData).subscribe(
        data => {
          this.movieCreated.emit(true);
          this.selectedFile = null;
          this.showMessage(true, data.response);
          this.clearForm();
        },
        error => {
          this.showMessage(false, 'Error interno al crearla.');
        }
      );

    } else {

      this.showMessage(false, 'Debes llenar todos los campos');
    }

  }

  formData() {
    const formData = new FormData();
    formData.append('code', this.formCreate.controls.code.value );
    formData.append('name', this.formCreate.controls.name.value );
    formData.append('description', this.formCreate.controls.description.value );
    formData.append("image", this.selectedFile);
    formData.append('cinema_id', this.formCreate.controls.cinema_id.value );
    return formData;
  }

  getImage(event){
    this.selectedFile = <File>event.target.files[0];
    this.loadImage(this.selectedFile);
  }

  loadImage (file) {
    const reader = new FileReader()
    reader.onload = (e: any) => {
      this.imageMin = e.target.result
    }
    reader.readAsDataURL(file)
  }  

  clearForm(){
    this.formCreate.reset();
  }

  showMessage(success: boolean, message: string = '') {

    if (success) {

      this.toastr.success(message, 'Película');

    } else {

      this.toastr.error(message, 'Película');
    }

  }  
  
  getCinemas(){
    this.cinemaService.getAll().subscribe(cinemas => {
      this.cinemas = cinemas.response
    });
  }

  get name(): any { return this.formCreate.get('name'); }
  get code(): any { return this.formCreate.get('code'); }
  get description(): any { return this.formCreate.get('description'); }
  get image(): any { return this.formCreate.get('image'); }
  get cinema_id(): any { return this.formCreate.get('cinema_id'); }  

}
