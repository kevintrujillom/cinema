import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  isUserLogin: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  readUserLogin(event: boolean){
    this.isUserLogin = event;
    this.emitChanges();
  }

  emitChanges(){
    setTimeout(() => {
      this.isUserLogin = false;
    }, 2000)
  }

}
