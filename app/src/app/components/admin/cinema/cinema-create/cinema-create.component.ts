import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Cinema } from './../../../../core/models/cinema.model';
import { CinemaService } from './../../../../core/services/cinema.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cinema-create',
  templateUrl: './cinema-create.component.html',
  styleUrls: ['./cinema-create.component.scss']
})
export class CinemaCreateComponent implements OnInit {

  @Output() cinemaCreated: EventEmitter<boolean> = new EventEmitter();

  formCreate: FormGroup;

  constructor( 
    private cinemaService: CinemaService,
    private toastr: ToastrService ) 
  {
    this.formCreate = this.createFormGroup();
  }

  ngOnInit(): void {
  }

  createFormGroup(){
    return new FormGroup({
      name: new FormControl('', [ Validators.required ]),
      capacity: new FormControl('', [ Validators.required ]),
    });
  }

  create(){

    if (this.formCreate.valid){

      this.cinemaService.create(this.formCreate.value).subscribe(
        data => {
          this.cinemaCreated.emit(true);
          this.showMessage(true, data.response);
          this.clearForm();
        },
        error => {
          this.showMessage(false, 'Error interno al crearla.');
        }
      );

    } else {

      this.showMessage(false, 'Debes llenar todos los campos');
    }

  }

  clearForm(){
    this.formCreate.reset();
  } 

  showMessage(success: boolean, message: string = '') {

    if (success) {

      this.toastr.success(message, 'Sala');

    } else {

      this.toastr.error(message, 'Sala');
    }

  }      

  get name(): any { return this.formCreate.get('name'); }
  get capacity(): any { return this.formCreate.get('capacity'); }

}
