<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Movie;

class ApiMoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = Movie::orderBy('id', 'DESC')->get();

        if ($movies) {

            return response()->json([ 
                'response' => $movies
            ], 200);

        }

        return response()->json([ 
            'response' => 'Sin resultados'
        ], 404);
    }

    public function list()
    {
        $movies = Movie::orderBy('id', 'DESC')->get();
        
        if ($movies) {
            
            foreach ($movies as $movie) {
                $movie->cinema;
            }

            return response()->json([ 
                'response' => $movies
            ], 200);

        }

        return response()->json([ 
            'response' => 'Sin resultados'
        ], 404);
    }    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('image');

        if ($file) {

            $destinationPath = 'images/';
            $filename = time() . '-' . $file->getClientOriginalName();
            $upload = $request->file('image')->move($destinationPath, $filename);
            $pathImage = $destinationPath . $filename;
        }

        if ($request->input('code') && $request->input('name') &&
            $request->input('description') && $pathImage &&
            $request->input('cinema_id')) {

            $movie = Movie::create([
                'code' => $request->input('code'),
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'image' => $pathImage,
                'cinema_id' => $request->input('cinema_id')
            ]);
            
            if ($movie) {
    
                return response()->json([ 
                    'response' => "{$movie->name}, guardada exitosamente."
                ], 200);
                
            } else {
    
                return response()->json([ 
                    'response' => "{$movie->name}, no guardada."
                ], 400);              
            }

        } else {

            return response()->json([ 
                'response' => "Campos vacíos."
            ], 400); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $movie = Movie::find($id);

        if ($movie) {

            $movie->cinema;

            return response()->json([
                'response' => $movie
            ], 200);

        }

        return response()->json([ 
            'response' => 'No existe esta sala.'
        ], 404);    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $file = $request->file('image');

        if ($file) {

            $destinationPath = 'images/';
            $filename = time() . '-' . $file->getClientOriginalName();
            $upload = $request->file('image')->move($destinationPath, $filename);
            $pathImage = $destinationPath . $filename;
        }

        $movie = Movie::find($id);

        if ($movie) {

            if (!isset($pathImage)) $pathImage = $movie->image;

            if ($request->input('code') && $request->input('name') &&
                $request->input('description') && $request->input('cinema_id')) {
    
                $movie->update([
                    'code' => $request->input('code'),
                    'name' => $request->input('name'),
                    'description' => $request->input('description'),
                    'image' => $pathImage,
                    'cinema_id' => $request->input('cinema_id')
                ]);
                
                if ($movie) {
        
                    return response()->json([ 
                        'response' => "{$movie->name}, actualizada exitosamente."
                    ], 200);
                    
                } else {
        
                    return response()->json([ 
                        'response' => "{$movie->name}, no actualizada."
                    ], 400);              
                }
    
            } else {
    
                return response()->json([ 
                    'response' => "Campos vacíos."
                ], 400); 
            }

        }

        return response()->json([ 
            'response' => "Pelicula con id {$id}, not existe"
        ], 400);  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $movie = Movie::find($id);

        if ($movie) {

            $movie->delete();

            if ($movie) {

                return response()->json([ 
                    'response' => "{$movie->name}, eliminada exitosamente."
                ], 200);

            } else {

                return response()->json([ 
                    'response' => "{$movie->name}, no eliminada."
                ], 400);                   
            }

        }

        return response()->json([ 
            'response' => "Pelicula con id {$id}, no existe."
        ], 404);    
    }
}
