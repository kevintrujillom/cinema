import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Cinema } from './../../../../core/models/cinema.model';
import { CinemaService } from './../../../../core/services/cinema.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-cinema-index',
  templateUrl: './cinema-index.component.html',
  styleUrls: ['./cinema-index.component.scss']
})
export class CinemaIndexComponent implements OnInit {

  cinemas: Cinema[] = [];
  cinemasOriginal = [];
  cinemasCopy = [];  
  actualPage: number = 1;

  search = '';
  activeSearch = false;
  cinemaCreated: boolean = false;

  formSearch: FormGroup;

  constructor( private cinemaService: CinemaService, private toastr: ToastrService ) { 
    this.formSearch = this.searchFormGroup();
  }  

  ngOnInit(): void {
    this.getAll();
  }

  ngOnChanges(): void {
    if (this.cinemaCreated) this.getAll();
  }   

  searchFormGroup(){
    return new FormGroup({
      name: new FormControl('', [ Validators.required ])
    });
  }

  getAll(){
    this.cinemaService.getAll().subscribe(cinemas => {
      this.cinemas = cinemas.response
    });
  }

  searchCinemas(){
    
    this.search = this.formSearch.value.name;

    if (this.activeSearch) this.cinemas = this.cinemasOriginal;

    if (this.search.length > 0) {

      this.createCopyCinemas();
  
      const nCinemas = this.cinemas.filter(cinema => {
        return cinema.name.toLowerCase().includes(this.search);
      });
  
      this.cinemas = nCinemas;

    } else {

      this.showMessage(false, 'Debes ingresar un parametro de búsqueda');
    }
    
  }  
  
  delete(cinema: Cinema){
    const response = confirm(`¿Seguro que desea borrar la película ${cinema.name}?`);
    if (response) this.confirmDelete(cinema.id);
  }

  confirmDelete(id: number){

    this.cinemaService.delete(id).subscribe(
      data => {
        this.showMessage(true, data.response);
        this.getAll();
      },
      error => {
        this.showMessage(false, 'Error interno al eliminarla.');
      }
    );

  }

  createCopyCinemas(){
    this.cinemasCopy = this.cinemas;
    this.cinemasOriginal = this.cinemas;
    this.activeSearch = true;
  }

  clearSearchCinemas() {
    this.cinemas = this.cinemasOriginal;
    this.activeSearch = false;
    this.search = '';
  }    

  showMessage(success: boolean, message: string = '') {

    if (success) {

      this.toastr.success(message, 'Sala');

    } else {

      this.toastr.error(message, 'Sala');
    }

  }
  
  readCinemaCreated(event: boolean){

    this.cinemaCreated = event;

    if (this.cinemaCreated) {
      this.getAll();
      this.emitChanges();
    }
  }

  emitChanges(){
    console.log('emitChanges readCinemaCreated');
    setTimeout(() => {
      this.cinemaCreated = false;
    },2000)
  }  

  get name(): any { return this.formSearch.get('name'); }

}
