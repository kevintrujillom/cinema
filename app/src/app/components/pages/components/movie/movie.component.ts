import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Movie } from './../../../../core/models/movie.model';
import { environment } from './../../../../../environments/environment';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {

  @Input() movie: Movie;
  @Input() carousel: boolean;
  @Output() movieAdded: EventEmitter<any> = new EventEmitter();
  urlImages = environment.path_images;

  constructor() { }

  ngOnInit(): void {
  }

  addMovie(){
    this.movieAdded.emit(this.movie.id)
  } 

}
