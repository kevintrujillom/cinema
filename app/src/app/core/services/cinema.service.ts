import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Cinema } from './../models/cinema.model';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CinemaService {

  constructor( private http: HttpClient ) { }

  getAll(): any {
    return this.http.get<Cinema[]>(`${environment.url_api}/cinemas/`)
  }

  getByid(id: number): any {
    return this.http.get<Cinema>(`${environment.url_api}/cinemas/${id}`)
  }

  create(cinema: Cinema): any {
    return this.http.post(`${environment.url_api}/cinemas`, cinema)
  }

  delete(id: number): any {
    return this.http.delete(`${environment.url_api}/cinemas/${id}`)
  }

  update(id: number, cinema: Cinema): any {
    return this.http.put(`${environment.url_api}/cinemas/${id}`, cinema)
  }  
}
