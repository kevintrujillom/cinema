# Cinema

# API (Backend)
```
Laravel: ^8.12
php: ^7.3 | ^8.0,
```

## Instalar dependencias
Una vez descargado el proyecto del repositorio se debe acceder al directorio llamado api/ donde se encuentra el API desarrollado con el framework Laravel de PHP. Ubicado en la carpeta se deben instalar las dependencias para ello se ejecuta el siguiente comando:
```
composer install
```
Crear un archivo llamado ".env" (lleva un punto al principio del nombre) en la raiz del directorio api/, para esto tomar como guía el archivo:
```
.env.example
```
Se debe ejecutar el siguiente comando para generar el 'APP_KEY' de la aplicación
```
php artisan key:generate
```

## Base de datos
Como motor de base de datos se implementó:
```
MySQL junto con phpmyadmin
```
En el archivo ".env" recientemente creado se debe ingresar la información de la base de datos

```
DB_DATABASE=nombre_base_de_datos
DB_USERNAME=usuario
DB_PASSWORD=contraseña
```
Para cargar algunos datos ('Usuarios', 'Salas', 'Películas') de prueba ejecutar el siguiente comando:
```
php artisan migrate:refresh --seed
```

## Iniciar servidor
Para iniciar el servidor ejecutar el siguiente comando:
```
php artisan serve
```
El API debe ser consumida a la siguiente url:
```
http://127.0.0.1:8000/api
```

# Aplicación (Frotend)
```
Angular CLI: 10.2.1
Node: 15.0.1
```
## Instalar dependencias
Una vez descargado el proyecto del repositorio se debe acceder al directorio llamado app/ donde se encuentra el frontend desarrollado con Angular. Ubicado en la carpeta se deben instalar las dependencias para ello se ejecuta el siguiente comando:
```
npm install
```
## Iniciar servidor
Para iniciar el servidor ejecutar el siguiente comando:
```
ng serve
```
En el navegador de preferencia ingresar el enlace para ver el aplicación:
```
http://localhost:4200/
```

## Nota:
Es importante tener en cuenta que la url para consumir el API es la siguiente (en esa url corre el API desarrollado con Laravel):
```
http://127.0.0.1:8000/api
```
Como se puede observar en el archivo environment.ts que se encuentra en la siguiente ubicación:
```
app/src/environments/environment.ts

```
# Instrucciones de la Aplicación:
## Para usuario administrador
Para poder acceder al dashboard administrativo usar las siguientes credenciales:
```
'email' = administrador@correo.com
'password' = carvajal2021
```
Estas son las acciones que puede realizar el administrador:
```
1. Crear salas de cine.
2. Ver en una tabla el listado de salas.
3. Buscar salas por su nombre.
4. Crear películas y asociarlas a una sala.
5. Ver en una tabla el listado de películas.
6. Buscar películas por su nombre.
7. Editar películas existentes.
8. Borrar películas existentes.
9. Ver en una tabla el listado de usuarios.
10. Buscar usuarios por su correo electrónico.
```
## Para usuario normal
Para poder iniciar sesión usar las siguientes credenciales:
```
'email' = usuario@correo.com
'password' = carvajal2021
```
Estas son las acciones que puede realizar el usuario normal:
```
1. Registrarse en la aplicación.
2. Navegar por el sitio y ver las películas en cartelera.
3. Ver en detalle la información completa de las películas.
4. Agregar a "Mi lista" las películas que desee (para esto debe iniciar sesión).
5. Ver un listado las películas que ha agregado (al haber iniciado sesión se le habilitará un item en el menú para poder acceder a esta página).
6. Eliminar las películas que desee de la lista.
```


# Tecnologías:
## Implementadas en el API (Backend):
```
1. Laravel (PHP)
2. MySQL (Motor de base de datos)
```
## Implementadas en la aplicación (Frotend):
```
1. Angular 10 (JS)
2. Angular Material (Estilos)
```

## Implementadas en el control de versiones:
```
1. Git
2. Bitbucket (repositorio)
```