import { Component, OnInit } from '@angular/core';

import { Movie } from './../../../core/models/movie.model';
import { MovieService } from './../../../core/services/movie.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  movies: Movie[] = [];

  actualPage: number = 1;

  constructor( private movieService: MovieService ) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(){
    this.movieService.getAll().subscribe(movies => {
      this.movies = movies.response
    });
  }

  addMovie() {

  }

}
