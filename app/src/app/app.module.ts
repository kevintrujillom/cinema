import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { MaterialModule } from './shared/modules/material/material.module';
import { ToastrModule } from 'ngx-toastr';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './components/layout/layout.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { HomeComponent } from './components/pages/home/home.component';
import { MovieDetailComponent } from './components/pages/movie-detail/movie-detail.component';
import { MovieComponent } from './components/pages/components/movie/movie.component';
import { CarouselComponent } from './components/pages/components/carousel/carousel.component';
import { CarouselMoviesComponent } from './components/pages/components/carousel-movies/carousel-movies.component';
import { LoginComponent } from './components/admin/login/login.component';
import { DashboardComponent } from './components/admin/dashboard/dashboard.component';
import { MovieCreateComponent } from './components/admin/movie/movie-create/movie-create.component';
import { MovieEditComponent } from './components/admin/movie/movie-edit/movie-edit.component';
import { MovieIndexComponent } from './components/admin/movie/movie-index/movie-index.component';
import { CinemaCreateComponent } from './components/admin/cinema/cinema-create/cinema-create.component';
import { CinemaIndexComponent } from './components/admin/cinema/cinema-index/cinema-index.component';
import { UserIndexComponent } from './components/admin/user/user-index/user-index.component';
import { CutStringPipe } from './core/pipes/cut-string.pipe';
import { ListaComponent } from './components/pages/lista/lista.component';
import { RegisterComponent } from './components/admin/register/register.component';
import { DialogComponent } from './components/shared/dialog/dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    MovieDetailComponent,
    MovieComponent,
    CarouselComponent,
    CarouselMoviesComponent,
    LoginComponent,
    DashboardComponent,
    MovieCreateComponent,
    MovieEditComponent,
    MovieIndexComponent,
    CinemaCreateComponent,
    CinemaIndexComponent,
    CutStringPipe,
    ListaComponent,
    RegisterComponent,
    UserIndexComponent,
    DialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,  
    HttpClientModule,  
    AppRoutingModule,
    NgxPaginationModule,
    CarouselModule,
    MaterialModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      progressBar: true,
      progressAnimation: 'increasing',
      preventDuplicates: true
    }),    
    NgxMatFileInputModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
