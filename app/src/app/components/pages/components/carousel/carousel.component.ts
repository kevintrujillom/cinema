import { Component, OnInit } from '@angular/core';

import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  customOptions: OwlOptions = {
    autoplay: true,
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    navText: ['Atras', 'Adelante'],
    responsive: {
      0: {
        items: 1
      }
    },
    nav: true
  }

  slides: any = [
    {
      name: 'combos',
      img: './assets/images/movies/combos.jpg'
    },
    {
      name: 'promos',
      img: './assets/images/movies/promos.jpg'
    }    
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
