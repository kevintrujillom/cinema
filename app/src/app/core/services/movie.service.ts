import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Movie } from './../models/movie.model';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor( private http: HttpClient ) { }

  getAll(): any {
    return this.http.get<Movie[]>(`${environment.url_api}/movies/`)
  }

  getList(): any {
    return this.http.get<Movie[]>(`${environment.url_api}/movies-lista/`)
  }  

  getByid(id: number): any {
    return this.http.get<Movie>(`${environment.url_api}/movies/${id}`)
  }

  create(movie: any): any {
    return this.http.post(`${environment.url_api}/movies`, movie)
  }

  delete(id: number): any {
    return this.http.delete(`${environment.url_api}/movies/${id}`)
  }

  /**
   * EN CASO DE ENVIAR UN ARCHIVO PARA ACTUALIZARLO
   * Se debe usar POST y enviar como parametro: '_method' = 'PUT'
   * dentro del FormData, esto debido a que el Symfony de Laravel,
   * no puede analizar los datos si son multipart/form-data.
   * 
   * SINO SE ENVIA UN ARCHIVO
   * Se puede usar de manera normal el metodo PUT. 
   */
  update(id: number, movie: any, hasFile: boolean): any {
    
    if (hasFile) {

      return this.http.post(`${environment.url_api}/movies/${id}`, movie)

    } else {

      return this.http.put(`${environment.url_api}/movies/${id}`, movie)
    }
  }

}
