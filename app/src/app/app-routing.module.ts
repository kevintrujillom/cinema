import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './core/guards/auth.guard';
import { AdminGuard } from './core/guards/admin.guard';

import { LayoutComponent } from './components/layout/layout.component';
import { HomeComponent } from './components/pages/home/home.component';
import { MovieDetailComponent } from './components/pages/movie-detail/movie-detail.component';
import { LoginComponent } from './components/admin/login/login.component';
import { RegisterComponent } from './components/admin/register/register.component';
import { DashboardComponent } from './components/admin/dashboard/dashboard.component';
import { MovieIndexComponent } from './components/admin/movie/movie-index/movie-index.component';
import { MovieEditComponent } from './components/admin/movie/movie-edit/movie-edit.component';
import { CinemaIndexComponent } from './components/admin/cinema/cinema-index/cinema-index.component';
import { UserIndexComponent } from './components/admin/user/user-index/user-index.component';
import { ListaComponent } from './components/pages/lista/lista.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'pelicula/:id',
        component: MovieDetailComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },

      {
        path: 'registrarse',
        component: RegisterComponent
      },      
      {
        path: 'dashboard',
        canActivate: [AdminGuard],
        component: DashboardComponent
      },
      {
        path: 'salas',
        canActivate: [AdminGuard],
        component: CinemaIndexComponent
      },
      {
        path: 'peliculas',
        canActivate: [AdminGuard],
        component: MovieIndexComponent
      },
      {
        path: 'peliculas/:id',
        canActivate: [AdminGuard],
        component: MovieEditComponent
      },
      {
        path: 'usuarios',
        canActivate: [AdminGuard],
        component: UserIndexComponent
      },       
      {
        path: 'mi-lista',
        canActivate: [AuthGuard],
        component: ListaComponent
      }
    ]
  }  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
