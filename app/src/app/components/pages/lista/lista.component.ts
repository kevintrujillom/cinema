import { Component, OnInit } from '@angular/core';

import { Movie } from './../../../core/models/movie.model';
import { MovieService } from './../../../core/services/movie.service';
import { ToastrService } from 'ngx-toastr';

import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from './../../shared/dialog/dialog.component';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.scss']
})
export class ListaComponent implements OnInit {

  movies= [];
  nList = [];
  list = JSON.parse(localStorage.getItem('movies_carvajal')) || false;

  constructor( 
    private movieService: MovieService,
    private toastr: ToastrService,
    public dialog: MatDialog ) { }

  ngOnInit(): void {
    this.getList();
  }

  getList(){
    this.movieService.getList().subscribe(movie => {
      this.movies = movie.response;
      this.existList();
    });
  }

  existList(){
    
    if (this.list) {
      
      this.nList = this.list.split("-");
      
      this.filterMovies();

    } else {

      this.movies = [];
      this.showMessage(true, 'No hay películas en tu lista.');
    }
    
  }

  filterMovies(){

    const nNumbers = this.nList.map(element => {
      return parseInt(element);
    })
    
    const nMovies = this.movies.filter(movies => {
      return nNumbers.includes(movies.id);
    });

    this.movies = nMovies;

  }

  delete(movie: Movie){
    const dialogRef = this.dialog.open(DialogComponent);
    dialogRef.afterClosed().subscribe(response => {
      if (response) this.confirmDelete(movie.id);
    });
  }

  confirmDelete(id: number){

    const nMovies = this.movies.filter(movies => {
      return movies.id != id;
    });

    this.movies = nMovies;

    this.setNewList();
  }  

  setNewList(){

    let newList = '';

    this.movies.forEach(element => {
      newList = newList + element.id + '-';
    });

    localStorage.setItem('movies_carvajal', JSON.stringify(newList));

  }

  showMessage(success: boolean, message: string = '') {

    if (success) {

      this.toastr.success(message, 'Película');

    } else {

      this.toastr.error(message, 'Película');
    }

  }  
}
