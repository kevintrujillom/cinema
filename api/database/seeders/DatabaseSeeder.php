<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            CinemaSeeder::class,
            MovieSeeder::class
        ]);

        \App\Models\User::create([
            'name' => 'Administrador',
            'email' => 'administrador@correo.com',
            'password' => \Hash::make('carvajal2021')
        ]);

        \App\Models\User::create([
            'name' => 'Usuario',
            'email' => 'usuario@correo.com',
            'password' => \Hash::make('carvajal2021')
        ]);      
    }
}
