export class Movie {
    id: number;
    code: string;
    name: string;
    description: string;
    image: string;
    cinema_id: number
}
