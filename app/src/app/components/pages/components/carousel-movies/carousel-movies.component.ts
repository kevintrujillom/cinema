import { Component, OnInit, Input } from '@angular/core';

import { Movie } from './../../../../core/models/movie.model';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-carousel-movies',
  templateUrl: './carousel-movies.component.html',
  styleUrls: ['./carousel-movies.component.scss']
})
export class CarouselMoviesComponent implements OnInit {

  customOptions: OwlOptions = {
    autoplay: true,
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    navText: ['Atras', 'Adelante'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      }
    },
    nav: true
  }
 
  @Input() movies: Movie[];

  constructor() { }

  ngOnInit(): void {
  }

  addMovie() {

  }

}
