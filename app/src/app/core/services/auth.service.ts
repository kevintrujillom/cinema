import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User } from './../models/user.model';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor( private http: HttpClient ) { }

  login(user: Partial<User>): any{
    return this.http.post(`${environment.url_api}/auth/login`, user)
  }

  logout(event: boolean): any{
    return this.http.post(`${environment.url_api}/auth/logout`, event)
  }  

  register(user: Partial<User>): any{
    return this.http.post(`${environment.url_api}/auth/register`, user)
  }    

}
