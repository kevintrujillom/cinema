import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from './../../../core/services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @Output() userLogin: EventEmitter<boolean> = new EventEmitter();

  formLogin: FormGroup;

  emailPattern: any = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

  constructor( 
    private authService: AuthService, 
    private toastr: ToastrService,
    private router: Router ) 
  { 
    this.formLogin = this.LoginFormGroup();
  }

  ngOnInit(): void {
    this.isLogin();
  }

  LoginFormGroup(){

    return new FormGroup({

      email: new FormControl('', [
        Validators.required,
        Validators.pattern(this.emailPattern)
      ]),

      password: new FormControl('', [
        Validators.required
      ])

    });

  }  

  isLogin(): void {

    const AuthUser = localStorage.getItem('user_carvajal');

    if (AuthUser) {
      this.router.navigate(['dashboard']);
    }
  }

  login(): void {

    if (this.formLogin.valid){

      this.authService.login(this.formLogin.value).subscribe(
        data => {
          if (data.isLogin) {
            // this.userLogin.emit(true);
            localStorage.setItem('user_carvajal', JSON.stringify(data.response));
            this.router.navigate(['dashboard']);
          }
        },
        error => {
          localStorage.removeItem('user_carvajal');
          this.showMessage(false, 'Datos de acceso incorrectos.');
        }
      );      

    } else {

      this.showMessage(false, 'Debes llenar todos los campos');
    }

  }

  showMessage(success: boolean, message: string = '') {

    if (success) {

      this.toastr.success(message, 'Login');

    } else {

      this.toastr.error(message, 'Login');
    }

  }    

  get email(): any { return this.formLogin.get('email'); }
  get password(): any { return this.formLogin.get('password'); }  

}
