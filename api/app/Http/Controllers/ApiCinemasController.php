<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Cinema;
use App\Models\Movie;

class ApiCinemasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Cinemas = Cinema::orderBy('id', 'DESC')->get();
        
        if ($Cinemas) {

            return response()->json([ 
                'response' => $Cinemas
            ], 200);

        }

        return response()->json([ 
            'response' => 'Sin resultados'
        ], 404);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->input('name') || $request->input('capacity')) {

            $cinema = Cinema::create([
                'name' => $request->input('name'),
                'capacity' => $request->input('capacity')
            ]);
            
            if ($cinema) {
    
                return response()->json([ 
                    'response' => "{$cinema->name}, guardada exitosamente."
                ], 200);
                
            } else {
    
                return response()->json([ 
                    'response' => "{$cinema->name}, no guardada."
                ], 400);              
            }

        } else {

            return response()->json([ 
                'response' => "Campos vacíos."
            ], 400); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cinema = Cinema::find($id);

        if ($cinema) {

            return response()->json([ 
                'response' => $cinema
            ], 200);

        }

        return response()->json([ 
            'response' => 'No existe esta sala.'
        ], 404); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cinema = Cinema::find($id);

        if ($cinema) {

            if ($request->input('name') && $request->input('capacity')) {
    
                $cinema->update([
                    'name' => $request->input('name'),
                    'capacity' => $request->input('capacity')
                ]);
                
                if ($cinema) {
        
                    return response()->json([ 
                        'response' => "{$cinema->name}, actualizada exitosamente."
                    ], 200);
                    
                } else {
        
                    return response()->json([ 
                        'response' => "{$cinema->name}, no actualizada."
                    ], 400);              
                }
    
            } else {
    
                return response()->json([ 
                    'response' => "Campos vacíos."
                ], 400); 
            }

        }

        return response()->json([ 
            'response' => "Sala con id {$id}, not existe."
        ], 400);    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cinema = Cinema::find($id);

        if ($cinema) {

            $products = Movie::where('cinema_id', '=', $id)->delete();
            
            $cinema->delete();

            if ($cinema) {

                return response()->json([
                    'response' => "Sala {$cinema->name}, eliminada exitosamente."
                ], 200);

            } else {

                return response()->json([ 
                    'response' => "{$cinema->name}, no eliminada."
                ], 400);                   
            }

        }

        return response()->json([ 
            'response' => "Sala con id {$id}, no existe."
        ], 404);   
    }
}
